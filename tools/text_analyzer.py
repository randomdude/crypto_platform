""" Text analysis class. Provides tools to analyze text on frequencies of characters
and other statistical tools.
"""


class TextAnalyzer:
    def __init__(self):
        self.alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                         'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        self.frequencies = []

    @staticmethod
    def extract_alphabet(text=''):
        pass

    def calculate_frequencies(self, text):
        pass

    def sort_asc(self):
        pass

    def sort_desc(self):
        pass

    def print_frequencies(self, use_percent=False):
        pass

    def get_frequency(self, character):
        pass
